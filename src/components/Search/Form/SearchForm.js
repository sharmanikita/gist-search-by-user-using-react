import React from 'react';
import { Image } from 'react-bootstrap';

import classes from './SearchForm.module.css';
import Button from '../../Button/Button';

const SearchForm = (props) => (
    <div>
        <header className={classes.Wrapper}>
            <Image className={classes.Logo} src={require('../../../assets/images/octocat.png')}/>
            <h1 className={classes.Sitename}>Github Gists</h1>
        </header>
        <main>
          <form className={classes.SearchForm} onSubmit={props.submitted}>

          <input
          placeholder='Enter username'
          value={props.value}
          onChange={props.changed}
          className={classes.InputField} />
        <Button type='Success' disabled={!props.formValid}>submit</Button>
        </form>
        </main>
</div>
);

export default SearchForm;