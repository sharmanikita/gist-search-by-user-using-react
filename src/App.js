import React, { Component } from 'react';

import GistsContainer from './containers/GistsContainer/GistsContainer';

class App extends Component {
    render() {
        return (
            <GistsContainer />
    );
    }
}

export default App;
